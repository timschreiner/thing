package com.timpo.thing;

import com.google.code.linkedinapi.client.LinkedInApiClient;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.schema.Connections;
import com.google.code.linkedinapi.schema.Person;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 *
 */
public class MetaFetcher {

  private final MetaParser parser;
  private final MetaTracker tracker;

  public MetaFetcher(MetaParser parser, MetaTracker tracker) {
    this.parser = parser;
    this.tracker = tracker;
  }

  public List<MetaCount> getMetaCounts(String consumerAPIKey,
          String consumerSecretValue,
          String accessTokenValue,
          String tokenSecretValue) {

    final LinkedInApiClientFactory factory = LinkedInApiClientFactory.newInstance(consumerAPIKey, consumerSecretValue);
    final LinkedInApiClient client = factory.createLinkedInApiClient(accessTokenValue, tokenSecretValue);

    Connections connections = client.getConnectionsForCurrentUser();
    for (Person person : connections.getPersonList()) {
      Collection<String> meta = parser.parseMeta(person);
      tracker.track(meta);
    }

    List<MetaCount> counts = tracker.getMetaCounts();
    Collections.sort(counts);
    return counts;
  }
}
