package com.timpo.thing;

import java.util.List;

/**
 * find interests of connections and get the total count of there interests
 */
public class Main {

  public static void main(String[] args) {

    String consumerAPIKey = "YOUR_KEY_HERE";
    String consumerSecretValue = "YOUR_SECRET_HERE";

    String accessTokenValue = "YOU_ACCESS_TOKEN_HERE";
    String tokenSecretValue = "YOUR_SECRET_TOKEN_HERE";

    MetaFetcher mf = new MetaFetcher(new MetaParser(), new MetaTracker());
    List<MetaCount> metaCounts = mf.getMetaCounts(consumerAPIKey, consumerSecretValue, accessTokenValue, tokenSecretValue);

    for (MetaCount metaCount : metaCounts) {
      System.out.println("metaCount = " + metaCount);
    }
  }
}
