package com.timpo.thing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
class MetaTracker {

  Map<String, MetaCount> interests;

  public MetaTracker() {
    interests = new HashMap<>();
  }

  void track(Collection<String> meta) {
    for (String metadata : meta) {
      MetaCount mc = interests.get(metadata);

      if (mc == null) {
        mc = new MetaCount(metadata);
        interests.put(metadata, mc);
      }

      mc.incrementCount();
    }
  }

  List<MetaCount> getMetaCounts() {
    return new ArrayList<>(interests.values());
  }

}
