package com.timpo.thing;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 */
class MetaCount implements Comparable<MetaCount> {

  private String metadata;
  private AtomicInteger count;

  public MetaCount(String metadata) {
    this.metadata = metadata;
    this.count = new AtomicInteger(0);
  }

  public int getCount() {
    return count.intValue();
  }

  public void incrementCount() {
    count.incrementAndGet();
  }

  public String getMetadata() {
    return metadata;
  }

  @Override
  public String toString() {
    return "MetaCount{" + "metadata=" + metadata + ", count=" + count.intValue() + '}';
  }

  @Override
  public int compareTo(MetaCount o) {
    return Integer.compare(o.getCount(), this.count.intValue());
  }
}
