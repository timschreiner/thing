package com.timpo.thing;

import com.google.code.linkedinapi.schema.Person;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Parses all the interesting information out of a Linkedin Contact
 */
class MetaParser {

  Collection<String> parseMeta(Person person) {
    Set<String> meta = new HashSet<>();

//    String associations = person.getAssociations();
//    System.out.println("associations = " + associations);
//    Certifications certifications = person.getCertifications();
//    System.out.println("certifications = " + certifications);
//    Educations educations = person.getEducations();
//    System.out.println("educations = " + educations);
//    String headline = person.getHeadline();
//    System.out.println("headline = " + headline);
    String industry = person.getIndustry();
//    System.out.println("industry = " + industry);
//    Languages languages = person.getLanguages();
//    System.out.println("languages = " + languages);
//    Location location = person.getLocation();
//    System.out.println("location = " + location);
//    Positions positions = person.getPositions();
//    System.out.println("positions = " + positions);
//    PersonActivities personActivities = person.getPersonActivities();
//    System.out.println("personActivities = " + personActivities);
//    System.out.println("\n\n\n");

    if (industry != null) {
      String[] split = industry.split(" ");

      meta.addAll(Arrays.asList(split));
    }

    return meta;
  }

}
